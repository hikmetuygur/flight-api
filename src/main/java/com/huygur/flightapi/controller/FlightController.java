package com.huygur.flightapi.controller;

import com.huygur.flightapi.controller.request.SearchRequest;
import com.huygur.flightapi.dto.FlightDTO;
import com.huygur.flightapi.service.FlightService;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/v1/flight")
public class FlightController {

    private final FlightService flightService;


    public FlightController(FlightService flightService) {
        this.flightService = flightService;

    }


    @GetMapping("/all")
    public List<FlightDTO> getAllFlights() {
        return flightService.getAllFlights();
    }


    @GetMapping("/{id}")
    public FlightDTO getFlightById(@PathVariable Long id) {
        return flightService.getFlightById(id);
    }

    @PostMapping("/create")
    public FlightDTO createFlight(@Valid @RequestBody FlightDTO flight) {
        return flightService.createFlight(flight);
    }

    @PutMapping("/{id}")
    public FlightDTO updateFlight(@PathVariable Long id, FlightDTO flight) {
        return flightService.updateFlight(id, flight);
    }

    @DeleteMapping("/{id}")
    public void deleteFlight(@PathVariable Long id) {
        flightService.deleteFlight(id);
    }

    @PostMapping("/search")
    public ResponseEntity<List<FlightDTO>> searchFlights(@Valid @RequestBody SearchRequest searchRequest) {
        return ResponseEntity.ok(flightService.searchFlights(searchRequest));
    }

}
